
var videoPlace = document.querySelector("#videoElement");

navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;

if(navigator.getUserMedia){
    navigator.getUserMedia({video:true}, handleVideo, videoError );
}

function handleVideo (stream){
    videoPlace.src = window.URL.createObjectURL(stream);
}


function videoError(e){
    console.log("Upss... Something gone wrong");
}
